
//Initialisierung von globalen Variablen
let started =  false;
let finished = false;
let firstTor = true;
let anzahlTore = 18;
let speed = 5;
let rekord = 0;
let rekordText = '';

//Aufruf der Funktion startScreen beim sobald seite geladen ist
$(document).ready(startScreen());

///Anzeige 'Drücke eine beliebige Taste'
function startScreen(){
	
	//Anzeige erzeugen und einfügen
	$start = $('<div id="start">Drücke eine beliebige Taste</div>');
	$('body').append($start);
	
	//Anzeige in die obere Rechte Ecke platzieren
	$start.get(0).style.top = 0.5* window.innerHeight - 0.5* $start.get(0).getBoundingClientRect().height + 'px';
}



//Tastendruck abfragen
$(document).keydown(function(event){
	

	//Wenn das Spiel gestartet ist, aber noch nicht fertig ist
	//Soll mit Pfeiltasten gesteuert werden
	if(started == true && finished == false){
		charBewegung(event.which);
	}
	
	//Wenn das Spiel noch nie gestartet wurde, soll jede Taste zum Start führen
	else if(started == false && finished == false){
		spielDurchlauf();
	}
	
	//Wenn das Spiel gestartet wurde, und dann beendet wurde, soll ein Tastendruck
	//zum Resetten und ablaufen des Spiels führen
	else if(started == true && finished == true){
		reset();
		spielDurchlauf();
	}
	
	

});



//Funktion, die bei Tastendruck aufgerufen wird, um den Skifahrer zu bewegen
//oder Spiel zu pausieren
function charBewegung(richtung){
	
	//Abstand des Skifahrers nach Links
	$skiData = $('#skifahrer').get(0).getBoundingClientRect();
	$left= $skiData.left;
		
		//Wenn die linke Pfeiltaste gedrückt wird, soll der Skifahrer 25px nach links
		if(richtung === 37){
			$left = $left - 25 + 'px';
			
			$('#skifahrer').css('left', $left);
		}
		
		//Wenn die rechte Pfeiltaste gedrückt wird, soll der Skifahrer 25px nach rechts
		else if(richtung === 39){
			$left = $left + 25 + 'px';
			$('#skifahrer').css('left', $left);
		}
		
		//Wenn die obere Pfeiltaste gedrückt wird, soll die Geschwindigkeit erhöht werden
		else if(richtung === 38){
			speed = speed + 1;
		}
		
		//Wenn die untere Pfeiltaste gedrückt wird, soll die Geschwindigkeit verringert werden
		else if(richtung === 40){
			if(speed > 1){
				speed = speed - 1;
			}
			
		}
	
}

//Funktion, mit der Objekte erzeugt und bewegt werden
function spielDurchlauf(){
	
	//Setze started auf wahr
	started = true;
	
	//'StartScreen'-Anzeige beim Starten des Spiels entfernen
	$('#start').remove();
	
	//Initialierung
	//Time ist die Spielzeit
	let time=0;
	let seconds = 0;
	let millis = 0;
	let startSeconds = new Date().getTime();
	let currentSeconds= 0;
	
	//i gibt die Anzahl der durchlaufenen Tore an
	//Gibt jedem Tor eine spezifische ID
	let i= 0;
	
	//Die Anfangsgeschwindigkeit in anfangsSpeed speichern
	let anfangsSpeed = speed;
	

	//Skifahrer wird erzeugt und eingefügt
	$skifahrer = $('<div id="skifahrer"></div>');
	$('body').append($skifahrer);
	
	//Testtor wird erzeugt und eingefügt
	//Wird benutzt um breite des Tors zu ermitteln, da die Startposition des Skifahrers mindestens eine halbe Torbreite vom Rand
	//entfernt sein muss
	$testTor =  $('<div class="tor">'+
			'<div class="torLinks">'+
			'</div><div class="torRechts">'+
		'</div></div>');
	$('body').append($testTor);
	
	//linke und rechte Grenze für mögliche Startposition des Skifahrers
	let startLinks = 0.5 * $('.tor').get(0).getBoundingClientRect().width;
	let startRechts = window.innerWidth - 0.5 * $('.tor').get(0).getBoundingClientRect().width 
						- $('#skifahrer').get(0).getBoundingClientRect().width ;
	
	//Zufallswert zwischen diesen Werten ist die Startposition
	let skiStart = rng(startLinks,startRechts );
	$('#skifahrer').css('left', skiStart );
	$testTor.remove();
	
	//Head mit Zeit, Punkten und Rekord erzeugen
	headErzeugen();
	
	//Intervall mit dem jede Millisekunde:
	//die Zeit und Punkte aktualisiert werden
	//Überprüft wird, ob ein Tor erzeugt werden muss
	let torSpawnTimer= setInterval(function(){
		
		//Differenz zwischen aktueller Zeit und Anfangszeit ist die Spielzeit
		if(finished === false){
			currentSeconds = new Date().getTime();
			time = currentSeconds - startSeconds;
		}
		
		//time ist aktuell nur in Millisekunden angegeben
		//Sekunden = Zeit/1000 auf nächst niedrigere Zahl gerundet
		//millis = Drei Nachkommastellen von time/1000 gerundet
		seconds = Math.floor(time/1000);
		millis = Math.round((time/1000 - Math.floor(time/1000))*1000);
		
		//millis so anpassen, dass es immer drei Nachkommastellen gibt
		if(millis.toString().length == 1){
			millis = '00' + (Math.round((time/1000 - Math.floor(time/1000))*1000)) ;
		}
		
		else if(millis.toString().length == 2){
			millis = '0' + (Math.round((time/1000 - Math.floor(time/1000))*1000)) ;
		}
		
		//In $zeit die Zeiten schreiben
		$zeit.text('Zeit: ' + seconds +'.'+ millis+' sek');
		//In $punkte den Wert von i schreiben
		$punkte.text('Punkte: ' + i);
		
		//If - Verzweigung, um zuüberprüfen, ob das Spiel vorbei ist
		if(finished == false){
			
			//Wenn firstTor wahr ist, soll ein Tor erzeugt werden ohne weiter Bediengung
			//und firstTor auf falsch gesetzt werden
			if(firstTor == true){
				
				torErzeugen();
				firstTor = false;
			}
			
			//Wenn das aktuelle Tor am Skifahrer vorbei ist, soll i hochgesetzt werden 
			//und ein neues Tor erzeugt werden
			else if(getTor(i).getBoundingClientRect().top < $('#skifahrer').get(0).getBoundingClientRect().top){

				i++;
				torErzeugen();		
			}
		}

		//Wenn das Spiel vorbei ist, sollen keine neuen Tore erzeugt werden
		else{
			
			clearInterval(torSpawnTimer);
		}

	}, 1);
	
	
	
	//Funktion zum erzeugen der Zeit-, Rekord- und Punkteanzeige 
	function headErzeugen(){
		
		//Zeit-, Rekord- und Punkteanzeige erzeugen und einfügen
		//bis auf das Ergebnis, welches erst eingeblendet wird, wenn das Spiel beendet ist
		
		//Zeitanzeige
		$zeit = $('<div id="zeit"></div>');
		$('body').append($zeit);
		
		//Rekordanzeige
		$rekord = $('<div id="rekord"></div>');
		$('body').append($rekord);
		
		//Wenn noch kein Rekord aufgestellt wurde, soll nur '00.000' angezeigt werden
		if(rekord === 0){
			
			$rekord.text('Rekord: 00.000 sek');
		}
		//Ansonsten der aktuelle Rekord
		else{
			$rekord.text(rekordText);
		}

		//Rekordanzeige in die obere Rechte Ecke setzen
		$('#rekord').get(0).style.left = window.innerWidth - $('#rekord').get(0).getBoundingClientRect().width +'px';
		
		
		//Punkteanzeige
		$punkte = $('<div id="punkte"></div>');
		$('body').append($punkte);
		$punkte.text('Punkte: ' + 0);
		
		//Punkteanzeige genau in die obere Mitte setzen
		$('#punkte').get(0).style.left = 0.5 * window.innerWidth - 0.5 * $('#punkte').get(0).getBoundingClientRect().width +'px';
	}
	

	

	
	
	//Funktion zum Erzeugen der Tore
	function torErzeugen(){
		
		//Wenn i kleiner als die Anzahl der Tore - 1 ist, dann soll ein 'normales' Tor erzeugt werden
		if(i < anzahlTore - 1){
			$tor = $('<div class="tor" id= '+ i +' >'+
					'<div class="torLinks">'+
					'</div><div class="torRechts">'+
				'</div></div>');

			$('body').append($tor);
	
		}
		
		//Wenn i gleich der Anzahl Tore - 1 ist, dann soll das Endtor erzeugt werden
		else{
			$endTor = $('<div class="endTor" id= '+ i +'></div>');
			$('body').append($endTor);
		}
		
		
		//Alle Tore ab dem ersten sollen am unteren Bildschirmrand erzeugt werden
		if(i > 0){
			getTor(i).style.top = '100%';
		}
		
		//Variablen, welche die einzelnen Daten des jeweiligen Tors beinhalten
		let $object = getTor(i);
		let $data =  $object.getBoundingClientRect();
		let $height = $data.height;
		let $pos = $data.top;
		let $width = $data.width;
		let $linksAbstand;
		
		//Die Position des ersten Tors soll abhängig von der Position des Skifahrers sein
		//Position des Skifahrers + breite des Skifahrers - halbe Torbreite 
		if(i === 0){
			
			$linksAbstand = $('#skifahrer').get(0).getBoundingClientRect().left + 
							0.5 * $('#skifahrer').get(0).getBoundingClientRect().width - 0.5 * $width;			
		}
		
		//Die Position aller weiteren Tore ist abhängig von der Position des jeweils vorherigen Tors
		//Die Differenz zwischen den Toren darf aber nur 2/3 der Torbreite betragen
		if(i > 0){

			//Mittelpunkt des vorherigen Tors bestimmen
			let $prevData = getTor(i-1).getBoundingClientRect();
			let $prevPos= $prevData.left + 0.5 * $data.width;
			
			//Grenzen festlegen zwischen denen die Position des nächsten Tors liegt
			let linkeGrenze = $prevPos - (2/3) * $data.width;
			let rechteGrenze = $prevPos + (2/3) * $data.width;
			linkeGrenze = linkeGrenze - 0.5 * $data.width;
			rechteGrenze = rechteGrenze - 0.5 * $data.width;
			
			//Zufallswert zwischen diesen Grenzen ermitteln
			$linksAbstand = rng(linkeGrenze, rechteGrenze );
			
		}
		
		//Tor position festlegen
		$object.style.left = $linksAbstand + 'px';
		
		//Breite eines Pfosten ermitteln
		let $torAußenWidth = $('.torLinks').get(0).getBoundingClientRect().width;
		
		//Bewegung der Tore wird durchgeführt
		let torAnimation = setInterval(function(){
			

			
			//Daten des Skifahrers ermitteln
			let $skiData = $('#skifahrer').get(0).getBoundingClientRect();
			let skiLeft = $skiData.left;
			let skiTop = $skiData.top;
			let skiRight = skiLeft + $skiData.width;
			let skiDown = skiTop + $skiData.height;
			
			//Daten des Tors mit der ID i ermitteln
			let $torData = getTor(i).getBoundingClientRect();
			let torLeft = $torData.left;
			let torTop = $torData.top;
			let torRight = torLeft + $torData.width;
			let torDown = torTop + $torData.height;
			
			
			//Wenn der Skifahrer im oder über dem Tor ist
			if(skiRight > torLeft && skiLeft < torRight){
				
				
				//Wenn das Tor mit der ID i ein normales Tor ist
				if(i < anzahlTore - 1){
					
					//Wenn nicht der Skifahrer zwischen den Pfosten ist
					//Alt: Wenn nicht die linke Seite des Skifahrers rechts vom linken Pfosten ist und die rechte Seite des Skifahrers
					//links vom rechten Pfosten ist					
					if(!(skiLeft > torLeft + $torAußenWidth && skiRight < torRight - $torAußenWidth)){
						
						//Wenn die untere Seite des Skifahrers zwischen der oberen und unteren Seite des Tores ist,
						//soll das Spiel beendet werden
						if(skiDown > torTop && skiDown < torDown ){
							
							//Versatz ausgleichen
							$pos = torTop - ( torTop - skiDown);
							
							spielBeendet();
							finished = true;
							
						}
					}
					
					
				}
			
				//Wenn das Tor mit der ID i das letzte Tor ist
				else if(i === anzahlTore - 1){
					
					//Wenn die untere Seite des Skifahrers zwischen der oberen und unteren Seite des Tores ist,
					//soll das Spiel beendet werden
					if(skiDown > torTop && skiDown < torDown ){	
						$pos = torTop - ( torTop - skiDown);
						i++;
						spielBeendet();
						finished = true;
						
						
					}
				}
				
				
			}
			
			
			//Wenn der Skifahrer den linken oder rechten Bildschirmrand übertritt
			else if(skiLeft < 0 || skiRight > window.innerWidth){
				$pos = torTop - ( torTop - skiDown);
				spielBeendet();
				finished = true;
				
			}
			
			//Wenn der Skifahrer innerhalb des Bildschirms, aber links oder rechts vom Tor vorbei geht
			else {
				
				if(skiDown> torTop && skiDown < torDown ){
					$pos = torTop - ( torTop - skiDown);
					spielBeendet();
					finished = true;
				}	
			}
			
			//Wenn ein Tor oben aus dem Bildschirm geht, soll es entfernt werden und die Animation dafür beendet werden
			if($pos + $height < 0){	
				clearInterval(torAnimation);
				$object.remove();	
				
			}
			
			//Wenn ein Tor noch innerhalb des Bildschirms ist
			else {
				
				//Es soll pro Tick um den Wert 'speed' nach oben verschoben werden
				$object.style.top = $pos + 'px';
				
				//Solange das Spiel noch nicht fertig ist, soll der Y - Wert immer weiter verringert werden
				if(finished === false){
					$pos = $pos - speed;
				}
				
				//Ist das Spiel beendet, soll auch keine Animation mehr ablaufen
				else {
					clearInterval(torAnimation);
				}
				
							
			}
			
			
		}, 10);
		
	}
		
	
	//Funktion, die Aktionen durchführt sobald das Spiel beendet ist
	function spielBeendet(){
		
		//Anzeige 'Drücke eine beliebige Taste' soll angezeigt werden
		startScreen();
		//Speed wird für den nächsten Durchlauf auf die Ursprungsgeschwindigkeit zurückgesetzt
		speed = anfangsSpeed;
		
		//Das Ergebnis soll erzeugt und eingefügt werden
		$ergebnis = $('<div id="ergebnis"></div>');
		$('body').append($ergebnis);
		
		
		//Wenn das Ziel erreicht wurde, soll 'Gewonnen' angezeigt werden
		if(i == anzahlTore){
			$ergebnis.text('Gewonnen!');
			
			//Wenn der vorherige Rekordwert größer als der aktuelle Wert ist, 
			//soll der aktuelle Wert zum rekord werden
			if(rekord > time && rekord !== 0){
				rekord = time;
				rekordText = 'Rekord: ' + seconds +'.'+ millis+' sek';
				$rekord.text(rekordText);
			}
			
			//Gab es vorher noch kein Rekord, soll der aktuelle Wert zum Rekord werden
			else if(rekord === 0){
				rekord = time;
				rekordText = 'Rekord: ' + seconds +'.'+ millis+' sek';
				$rekord.text(rekordText);
			}
			
			
			
		}
		
		//Wenn das Ziel nicht erreicht wurde, soll 'Ouch!' ausgegeben werden
		else{
			$ergebnis.text('Ouch!');
		}
		
		
		//Ergebnisanzeige fixen, falls Skifahrer nah an einer der Bildschirmgrenzen ist

		//Wenn die Position des Skifahrers + halbe Breite des Skifahrer - halbe Breite des Ergebnisse kleiner als 0 ist
		//soll das Ergebnis an die Position 0 geschrieben werden
		if($('#skifahrer').get(0).getBoundingClientRect().left - 0.5* $('#ergebnis').get(0).getBoundingClientRect().width
				+ 0.5* $('#skifahrer').get(0).getBoundingClientRect().width < 0){
			
			$('#ergebnis').get(0).style.left = '0px';
			
			
		}
		
		//Wenn die Position des Skifahrers + halbe Breite des Skifahrer + halbe Breite des Ergebnisse größer als das Fenster ist
		//soll das Ergebnis an die Position des rechten Fensterrands - halbe Breite geschrieben werden
		else if($('#skifahrer').get(0).getBoundingClientRect().left + 0.5* $('#ergebnis').get(0).getBoundingClientRect().width 
				+ 0.5* $('#skifahrer').get(0).getBoundingClientRect().width > window.innerWidth){
			
			$('#ergebnis').get(0).style.left = window.innerWidth - $('#ergebnis').get(0).getBoundingClientRect().width +'px';
		}
		
		//Ansonsten soll das Ergebnis mittig über dem Skifahrer stehen
		else{
			$('#ergebnis').get(0).style.left = $('#skifahrer').get(0).getBoundingClientRect().left - 
			0.5* $('#ergebnis').get(0).getBoundingClientRect().width 
			+ 0.5* $('#skifahrer').get(0).getBoundingClientRect().width + 'px';
		}
		
		//Das Ergebnis soll von der Höhe her direkt über dem Skifahrer stehen
		$('#ergebnis').get(0).style.top = $('#skifahrer').get(0).getBoundingClientRect().top - $('#ergebnis').get(0).getBoundingClientRect().height+'px';
		
		
		
	}
		

		
}

//Funktion mit der eine Zufallszahl zwischen zwei Grenzen erzeugt wird
function rng(min, max){
	
	//Wenn die rechte Grenze größer als der rechte Fensterrand ist, soll die Grenze auf den rechten Fensterrand gesetzt werden
	if(max > window.innerWidth - $('.tor').get(0).getBoundingClientRect().width){
		max = window.innerWidth - $('.tor').get(0).getBoundingClientRect().width;
		
	}
	
	//Wenn die linke Grenze kleiner als der linke Fensterrand ist, soll die Grenze auf den linken Fensterrand gesetzt werden
	if(min < 0){
		min = 0;
	}
	
	//Zufallszahl zwischen 0 und 1 wird mit der Differenz zwischen den Grenzen multipliziert und dann muss noch die untere Grenze addiert
	//werden, damit die Zahl noch im Intervall liegt
	rueckgabe = Math.random()*(max-min)+min;
	return rueckgabe;
}

//Funktion, der eine ID übergeben wird und das entsprechende DOM - Objekt zurückliefert
function getTor(id){
	
	$tor = $('#'+ id +'').get(0);
	
	return $tor;
	
}

//Funktion, die alle Werte zurücksetzt,die während eines Durchlaufs verändert werden
//! bis auf den Wert rekord
function reset(){
	
	started = false;
	finished = false;
	letztesTor = false;
	firstTor = true;
	$('.endTor').remove();
	$('.tor').remove();
	$('#skifahrer').remove();
	$('#punkte').remove();
	$('#ergebnis').remove();
	$('#zeit').remove();
	$('#rekord').remove();
	
	
}

